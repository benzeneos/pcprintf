# PCPrintf
**PCPrintf** (Put Character Printf) is a printf implementation targetting
embedded systems. It does not require any buffers (except placing some `int`s
on stack), `malloc`, `stdio` or so on.

## Features

### Format conversions

 * `d`, `i` - format a signed integer
 * `u` - format an unsigned integer
 * `x` - format an unsigned hexadecimal integer
 * `s` - output a string
 * `c` - output a character
 * `%` - output a pure percent

### Numeric sizes

 * `hh`: uses `char`s instead of `int`s (note: this prints ASCII codes, not
   actual printable characters)
 * `h`: uses `short`s instead of `int`s
 * `l`: uses `long`s instead of `int`s
 * `ll`, `L`: uses `long long`s instead of `int`s
 * `j`: uses `intmax_t` and `uintmax_t`
 * `z`: uses `ssize_t` and `size_t`
 * `t`: uses `ptrdiff_t` (currently ignores unsigned formats)
