/*
 * Copyright (C) BenzeneOS Team
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <pcprintf.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>

#define test_snippet() do \
{ \
  printf ("printf (\"%%u\", 3): %u\n", 3); \
  printf ("printf (\"Hello, %%s!\", \"World\"): Hello, %s!\n", "World"); \
  printf ("printf (\"%%llu\", ULLONG_MAX): %llu\n", ULLONG_MAX); \
  printf ("printf (\"%%u %%u\", 123, 456): %d %d\n", 123, 456); \
  printf ("printf (\"%%0#10lx\", 0xbeef): %0#10lx\n", 0xbeef); \
} while (0)

int
main (int argc, char **argv)
{
  printf ("==== Using system printf! ====\n");
  test_snippet ();
  printf ("==== Using pcprintf with system putchar! ====\n");
#define printf(x, ...) pcprintf (putchar, x, ##__VA_ARGS__)
  test_snippet ();
  return 0;
}
