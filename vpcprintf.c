/*
 * Copyright (C) BenzeneOS Team
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <config.h>

#include <pcprintf.h>

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

static bool
x_isdigit (char ch)
{
  return ch >= '0' && ch <= '9';
}

typedef long ssize_t;

static const char INT_TO_CHAR_TABLE[] = {
  '0', '1', '2', '3', '4', '5', '6', '7',
  '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
};

static int CHAR_TO_INT_TABLE[128] = {
};

static bool ch_to_i_init_done;

static void
init_ch_to_i (void)
{
  if (ch_to_i_init_done)
    return;
  ch_to_i_init_done = true;
  for (int i = 0; i < 128; i++)
    CHAR_TO_INT_TABLE[i] = -1;
  for (int i = 0; i < 10; i++)
    CHAR_TO_INT_TABLE['0' + i] = i;
}

static int
x_atoi (const char **str)
{
  int result = 0;

  while (**str)
    {
      if (!x_isdigit (**str) || CHAR_TO_INT_TABLE[**str] == -1)
        break;
      result *= 10;
      result += CHAR_TO_INT_TABLE[**str];
      (*str)++;
    }
  
  return result;
}

#define PUTC(ch) \
  if (fn (ch) == -1) \
    { \
      return -1; \
    } \
  else \
    { \
      written_count ++; \
    }

typedef enum
{
  FMT_SIZE_NONE,
  FMT_SIZE_HH,
  FMT_SIZE_H,
  FMT_SIZE_L,
  FMT_SIZE_LL,
  FMT_SIZE_J,
  FMT_SIZE_Z,
  FMT_SIZE_T,
} fmt_size;

typedef struct
{
  int radix;
  unsigned int field_width;
  bool alt_form;
  bool zeropad;
  bool leftpad;
  bool nonnegspace;
  bool forcesign;
  int prec;
  fmt_size size;
} fmt_args;

#define func(n,t) \
  static int \
  n (int *written_count_ptr, pcprintf_putc_func fn, t val, \
     const fmt_args *fmtarg);
#include "ifmt-funcs.h"
#undef func

static void
fmt_args_reset (fmt_args *ag)
{
  /* Reset fields to default values */
  ag->radix = 10;
  ag->field_width = 0;
  ag->alt_form = false;
  ag->zeropad = false;
  ag->leftpad = false;
  ag->nonnegspace = false;
  ag->forcesign = false;
  ag->prec = 0;
  ag->size = FMT_SIZE_NONE;
}

static int
read_format (int *written_count_ptr, pcprintf_putc_func fn, const char *fmt,
             va_list ap);

PCPRINTF_ATTR_FORMAT (2, 0) int
vpcprintf (pcprintf_putc_func fn, const char *fmt, va_list ap)
{
  int written_count = 0;
  int x;

  while (*fmt)
    {
      switch (*fmt)
        {
        case '%':
          x = read_format (&written_count, fn, fmt, ap);
          if (x == -1)
            return -1;
          fmt += x;
          break;
        default:
          PUTC (*fmt);
          fmt++;
          break;
        }
    }

  return written_count;
}

/* Functions down there get written_count as a pointer */
#define written_count (*written_count_ptr)

#define write_all() \
do \
  { \
    const char *itr2 = fmt; \
    do \
      { \
        fn (*itr2); \
        itr2++; \
      } \
    while (itr2 < itr); \
  } \
while (0)

static int
read_format (int *written_count_ptr, pcprintf_putc_func fn, const char *fmt,
             va_list ap)
{
  fmt_args fmtarg;
  fmt_size fmtsize;

  fmt_args_reset (&fmtarg);

  const char *itr = fmt + 1;
  if (!*itr)
    {
      write_all ();
      return -1;
    }
  char buf[8];
  while (1)
    {
      switch (*itr)
        {
        case 0:
          write_all ();
          return -1;
        case '0':
          if (fmtarg.zeropad)
            goto after_flag_loop;
          fmtarg.zeropad = true;
          break;
        case '#':
          fmtarg.alt_form = true;
          break;
        case '-':
          fmtarg.leftpad = true;
          break;
        case ' ':
          fmtarg.nonnegspace = true;
          break;
        case '+':
          fmtarg.forcesign = true;
        default:
          goto after_flag_loop;
          break;
        }
      itr++;
    }
after_flag_loop:;
  fmtarg.field_width = x_atoi (&itr);
  fmtarg.prec = -2;
  if (*itr == '.')
    {
      itr++;
      if (x_isdigit (*itr))
        {
          fmtarg.prec = x_atoi (&itr);
        }
      else if (*itr == '*')
        {
          // Todo: *m$ (argument indexes are not handled yet)
          fmtarg.prec = -1;
          itr++;
        }
      else
        {
          fmtarg.prec = 0;
        }
    }
  fmtarg.size = FMT_SIZE_NONE;
  while (true)
    {
      switch (*itr)
        {
        case 'h':
          if (fmtarg.size == FMT_SIZE_H)
            fmtarg.size = FMT_SIZE_HH;
          else if (fmtarg.size == FMT_SIZE_NONE)
            fmtarg.size = FMT_SIZE_H;
          else
            {
              write_all ();
              return -1;
            }
          break;
        case 'l':
          if (fmtarg.size == FMT_SIZE_L)
            fmtarg.size = FMT_SIZE_LL;
          else if (fmtarg.size == FMT_SIZE_NONE)
            fmtarg.size = FMT_SIZE_L;
          else
            {
              write_all ();
              return -1;
            }
          break;
        case 'L':
          if (fmtarg.size == FMT_SIZE_NONE)
            fmtarg.size = FMT_SIZE_LL;
          else
            {
              write_all ();
              return -1;
            }
          break;
        case 'j':
          if (fmtarg.size == FMT_SIZE_NONE)
            fmtarg.size = FMT_SIZE_J;
          else
            {
              write_all ();
              return -1;
            }
          break;
        case 'z':
          if (fmtarg.size == FMT_SIZE_NONE)
            fmtarg.size = FMT_SIZE_Z;
          else
            {
              write_all ();
              return -1;
            }
          break;
        case 't':
          if (fmtarg.size == FMT_SIZE_NONE)
            fmtarg.size = FMT_SIZE_T;
          else
            {
              write_all ();
              return -1;
            }
          break;
        default:
          goto after_size_loop;
        }
      itr++;
    }
after_size_loop:;
  switch (*itr)
    {
    case 'i':
    case 'd':
      fmtarg.radix = 10;
      switch (fmtarg.size)
        {
        case FMT_SIZE_NONE:
          fmt_int (written_count_ptr, fn, va_arg (ap, int), &fmtarg);
          break;
        case FMT_SIZE_HH:
          fmt_char (written_count_ptr, fn, va_arg (ap, int), &fmtarg);
          break;
        case FMT_SIZE_H:
          fmt_short (written_count_ptr, fn, va_arg (ap, int), &fmtarg);
          break;
        case FMT_SIZE_L:
          fmt_long (written_count_ptr, fn, va_arg (ap, long), &fmtarg);
          break;
        case FMT_SIZE_LL:
          fmt_llong (written_count_ptr, fn, va_arg (ap, long long), &fmtarg);
          break;
        case FMT_SIZE_J:
          fmt_intmax_t (written_count_ptr, fn, va_arg (ap, intmax_t), &fmtarg);
          break;
        case FMT_SIZE_Z:
          fmt_ssize_t (written_count_ptr, fn, va_arg (ap, ssize_t), &fmtarg);
          break;
        case FMT_SIZE_T:
          fmt_ptrdiff_t (written_count_ptr, fn, va_arg (ap, ptrdiff_t), &fmtarg);
          break;
        }
      itr++;
      break;
    case 'u':
      fmtarg.radix = 10;
      switch (fmtarg.size)
        {
        case FMT_SIZE_NONE:
          fmt_uint (written_count_ptr, fn, va_arg (ap, unsigned int), &fmtarg);
          break;
        case FMT_SIZE_HH:
          fmt_uchar (written_count_ptr, fn, va_arg (ap, unsigned int),
                     &fmtarg);
          break;
        case FMT_SIZE_H:
          fmt_ushort (written_count_ptr, fn, va_arg (ap, unsigned int),
                      &fmtarg);
          break;
        case FMT_SIZE_L:
          fmt_ulong (written_count_ptr, fn, va_arg (ap, unsigned long),
                     &fmtarg);
          break;
        case FMT_SIZE_LL:
          fmt_ullong (written_count_ptr, fn, va_arg (ap, unsigned long long),
                      &fmtarg);
          break;
        case FMT_SIZE_J:
          fmt_uintmax_t (written_count_ptr, fn, va_arg (ap, uintmax_t),
                         &fmtarg);
          break;
        case FMT_SIZE_Z:
          fmt_size_t (written_count_ptr, fn, va_arg (ap, size_t), &fmtarg);
          break;
        case FMT_SIZE_T:
          fmt_ptrdiff_t (written_count_ptr, fn, va_arg (ap, ptrdiff_t),
                         &fmtarg);
          break;
        }
      itr++;
      break;
    case 'x':
      fmtarg.radix = 16;
      switch (fmtarg.size)
        {
        case FMT_SIZE_NONE:
          fmt_uint (written_count_ptr, fn, va_arg (ap, int), &fmtarg);
          break;
        case FMT_SIZE_HH:
          fmt_uchar (written_count_ptr, fn, va_arg (ap, int), &fmtarg);
          break;
        case FMT_SIZE_H:
          fmt_ushort (written_count_ptr, fn, va_arg (ap, int), &fmtarg);
          break;
        case FMT_SIZE_L:
          fmt_ulong (written_count_ptr, fn, va_arg (ap, long), &fmtarg);
          break;
        case FMT_SIZE_LL:
          fmt_ullong (written_count_ptr, fn, va_arg (ap, long long), &fmtarg);
          break;
        case FMT_SIZE_J:
          fmt_uintmax_t (written_count_ptr, fn, va_arg (ap, intmax_t), &fmtarg);
          break;
        case FMT_SIZE_Z:
          fmt_size_t (written_count_ptr, fn, va_arg (ap, ssize_t), &fmtarg);
          break;
        case FMT_SIZE_T:
          fmt_ptrdiff_t (written_count_ptr, fn, va_arg (ap, ptrdiff_t), &fmtarg);
          break;
        }
      itr++;
      break;
    case 's':
      {
        const char *str = va_arg (ap, const char *);
        if (!str)
          str = "(null)";
        while (*str)
          PUTC (*str++);
        itr++;
        break;
      }
    case 'c':
      PUTC (va_arg (ap, int));
      itr++;
      break;
    case '%':
      PUTC ('%');
      itr++;
      break;
    default:
      break;
    }
  return itr - fmt;
}

#undef write_all

#define func(n, t) \
  static int \
  n (int *written_count_ptr, pcprintf_putc_func fn, t val, \
     const fmt_args *fmtarg) \
  { \
    if (val == 0) \
      { \
        if (fmtarg->leftpad) \
          { \
            char ch = fmtarg->zeropad ? '0' : ' '; \
            for (int i = (fmtarg->forcesign || fmtarg->nonnegspace) \
                 ? 2 : 1; i < fmtarg->field_width; i++) \
              { \
                PUTC (ch); \
              } \
          } \
        if (fmtarg->forcesign) \
          { \
            PUTC ('+'); \
          } \
        else if (fmtarg->nonnegspace) \
          { \
            PUTC (' '); \
          } \
        PUTC ('0'); \
        if (!fmtarg->leftpad) \
          { \
            char ch = fmtarg->zeropad ? '0' : ' '; \
            for (int i = (fmtarg->forcesign || fmtarg->nonnegspace) \
                 ? 2 : 1; i < fmtarg->field_width; i++) \
              { \
                PUTC (ch); \
              } \
          } \
        return 0; \
      } \
    /* We can not allocate stuff, so no tricks with string reversing should
       be made. Instead, let's play it fair. */ \
    int digits = 0; \
    int digits_add = 0; \
    if (val < 0 || fmtarg->forcesign || fmtarg->nonnegspace) \
      { \
        digits_add ++; \
      } \
    if (val < 0) \
      { \
        val = -val; \
      } \
    int digits_all = digits + digits_add; \
    { \
      t copy = val; \
      while (copy) \
        { \
          digits++; \
          if (copy < fmtarg->radix) \
            { \
              copy = ((t) 0); \
            } \
          else \
            { \
              copy /= fmtarg->radix; \
            } \
        } \
    } \
    const char *alt_form_str = ""; \
    if (fmtarg->alt_form) \
      { \
        switch (fmtarg->radix) \
          { \
          case 8: \
            alt_form_str = "0"; \
            digits_add++; \
            break; \
          case 2: \
            alt_form_str = "0b"; \
            digits_add += 2; \
            break; \
          case 16: \
            alt_form_str = "0x"; \
            digits_add += 2; \
            break; \
          } \
      } \
    if (fmtarg->leftpad) \
      { \
        char ch = fmtarg->zeropad ? '0' : ' '; \
        for (int i = digits_all; i < fmtarg->field_width; i++) \
          { \
            PUTC (ch); \
          } \
      } \
    if (val < 0) \
      { \
        PUTC ('-'); \
      } \
    else if (fmtarg->forcesign) \
      { \
        PUTC ('+'); \
      } \
    else if (fmtarg->nonnegspace) \
      { \
        PUTC (' '); \
      } \
    while (*alt_form_str) \
      { \
        PUTC (*alt_form_str); \
        alt_form_str ++; \
      } \
    for (int i = 0; i < digits; i++) \
      { \
        t copy = val; \
        for (int j = digits - 1; j > i; j--) \
          { \
            copy /= fmtarg->radix; \
          } \
        PUTC (INT_TO_CHAR_TABLE[copy % fmtarg->radix]); \
      } \
    if (!fmtarg->leftpad) \
      { \
        char ch = fmtarg->zeropad ? '0' : ' '; \
        for (int i = digits_all; i < fmtarg->field_width; i++) \
          { \
            PUTC (ch); \
          } \
      } \
    return 0; \
  }
#include "ifmt-funcs.h"
#undef func
