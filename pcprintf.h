/*
 * Copyright (C) 2016 Jakub Kaszycki
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _PCPRINTF_H
#define _PCPRINTF_H

#include <stdarg.h>

#ifdef __GNUC__
#define PCPRINTF_ATTR_FORMAT(x, y) __attribute__ ((__format__ (printf, x, y)))
#else
#define PCPRINTF_ATTR_FORMAT(x, y)
#endif

#ifdef __cplusplus
#define PCPRINTF_BEGIN_DECLS extern "C" {
#define PCPRINTF_END_DECLS }
#else
#define PCPRINTF_BEGIN_DECLS
#define PCPRINTF_END_DECLS
#endif

/* pcprintf - a printf() with a putchar function as only argument */

PCPRINTF_BEGIN_DECLS

/**
 * A character-put functions.
 * 
 * @param ch character to put
 * @returns 0 if successful, -1 otherwise
 */
typedef int (*pcprintf_putc_func) (int ch);

/**
 * Prints a string.
 * 
 * @param putc character put function
 * @param fmt format string
 * @param ... arguments to format
 * @returns written character count or negative on error
 */
PCPRINTF_ATTR_FORMAT (2, 3)
int pcprintf (pcprintf_putc_func putc, const char *fmt, ...);

/**
 * Prints a string.
 * 
 * @param putc character put function
 * @param fmt format string
 * @param ap arguments to format
 * @returns written character count or negative on error
 */
PCPRINTF_ATTR_FORMAT (2, 0)
int vpcprintf (pcprintf_putc_func putc, const char *fmt, va_list ap);

PCPRINTF_END_DECLS

#endif
